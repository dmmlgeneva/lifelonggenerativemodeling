# Lifelong Generative Modeling
Implementation and code for Lifelong Generative Modeling with VAE's.

## Run the code
Be sure to checkout the entire repo (including submodules):
```bash
git clone --recursive https://bitbucket.org/dmmlgeneva/lifelonggenerativemodelingicml
```

The authoritative code is the tensorflow code, but the FID code and better dataset aggregation is implemented in pytorch.

### Examples Pytorch

For lifelong:

```bash
python main.py --calculate-fid --batch-size=300 --reparam-type="mixture" --discrete-size=1 --continuous-size=14 --epochs=400 --layer=dense --optimizer=adam --mut-reg=1.0 --task fashion --uid=fashionlifelong
```

For ewc:

```bash
python main.py --calculate-fid --batch-size=300 --reparam-type="isotropic_gaussian" --continuous-size=34 --epochs=400 --layer=dense --optimizer=adam --mut-reg=0.0 --disable-regularizers --ewc --task fashion --uid=fashionewc
```

Vanilla w/ same graphical model:

```bash
python main.py --calculate-fid --batch-size=300 --reparam-type="mixture" --discrete-size=1 --continuous-size=14 --epochs=400 --layer=dense --optimizer=adam --mut-reg=0.0 --disable-regularizers --task fashion --uid=fashionvanilla
```

Vanilla w/ same gaussian (true vanilla) :

```bash
python main.py --calculate-fid --batch-size=300 --reparam-type="mixture" --continuous-size=34 --epochs=400 --layer=dense --optimizer=adam --mut-reg=0.0 --disable-regularizers --task fashion --uid=fashionvanillaisotropic
```

### Examples tensorflow

Lifelong :

```bash
python run_fashion_dnn_experiment.py --base_dir="<LOCATION_TO_SAVE_EXPERIMENTS>" --device="/gpu:0" --sequential=True --device_percentage=0.9 --latent_size=14 --use_bn=True --min_interval=40000 --max_dist_swaps=10 --mutual_info_reg=0.05 --batch_size=256
 ```
