\vspace{-0.2in}
\section{Introduction}
\vskip -0.1in
Deep unsupervised generative learning allows us to take advantage of the massive
amount of unlabeled data available in order to build models that efficiently
compress and learn an approximation of the true data distribution. It
has numerous applications such as image denoising, inpainting, super-resolution, structured
prediction, clustering, pre-training and many more. However, something that is
lacking in the modern ML toolbox is an efficient way to learn these deep
generative models in a lifelong setting.
In a lot of real world scenarios we observe distributions
sequentially; children in elementary school for example learn the
alphabet letter-by-letter and in a sequential manner. % We might
% not be able to sample data from all of the possible underlying distributions
% simultaneously as this would require estimation and storage of each of these
% disjoint distributions. % This might be due to the fact that the number of tasks grows
% over time or that we simply do not have access to all of them concurrently.
Other real world examples include video data from sensors such as cameras and
microphones or other similar sequential data. A system can also be resource
limited wherein all of the past data or learnt models cannot be
stored. The navigation of a resource limited robot in an unknown environment for instance, might require the
robot to be able to inpaint images from a learnt generative model in a
previous environment\footnote{This setting is drastically different from the online
  learning setting; we touch upon this in Appendix \ref{diff_streaming}}.

% We are interested in the lifelong learning setting for generative models
% where distributions arrives sequentially and where the storage of all data
% is infeasible.
% In this work we \textbf{assume} that instances at any
% time are generated according to some non-observed distribution and
% that distributional transitions are \textbf{known}.
% We provide
% an example of such a setting in figure \ref{fig_problem_setup}(a) using
% MNIST \cite{lecun-mnisthandwrittendigit-2010}, where we sequentially
% observe three distributions.
% During our
% schooling years we are generally provided with such information; as an
% example children are told taught capital letters
% We posit that this is a reasonable assumption; during the learning of
% the alphabet for example, children are taught capital letters after which their
% teacher instructs them on lower case characters. Here, the
% distribution transition is directly provided.

% Within the stream, instances are generated according to some
% non-observed distribution which changes at given time-points.
% we assume we know the time points at which the transitions occur and whether the
% latent distribution is a completely new one or one that has been observed
% before.
% We do not however know the underlying identity of the individual
% distributions.

% While learning the transition intervals is possible we leave this
% future work in order to avoid compounding errors. Our goal is to learn a generative
% model that can summarize all the observed distributions.
% We provide
% an example of such a setting in figure \ref{fig_problem_setup}(a) using
% MNIST \cite{lecun-mnisthandwrittendigit-2010}, where we sequentially
% observe three distributions.
% we have three unique distributions and one that is repeated.

% We focus on the Variational Autoencoder \cite{kingma2014} (VAE)
% as it allows us to model an approximate posterior of our data distribution via
% variational inference. This allows for the option of leveraging our model for tasks such as
% unsupervised learning or even semi-supervised learning \cite{xu2017variational}.
% Variational Autoencoders allow us to generate data by sampling the prior and
% running the sample through the decoder. In addition, smooth translations in the
% latent space allow for generation of novel data samples.

In the lifelong learning % (also known as continual learning)
setting % for generative models
% where
we sequentially observe a single distribution at a time from a possibly
infinite set of distributions% and where the storage of all of the
% observed data is infeasible
. Our objective is to learn a \emph{single model} that
is able to generate from \emph{each} of the individual distributions \emph{without the
preservation of the observed data}.
% Such a setting also implies that the storage of all of the observed data is infeasible.
We provide an example of such a setting in figure \ref{fig_problem_setup}(a) using
MNIST \cite{lecun-mnisthandwrittendigit-2010}, where we sequentially
observe three distributions.
Since we only observe one distribution at a time
we need to develop a strategy of retaining the previously learnt distributions and integrating it into future learning.
To accumulate additional distributions in the current generative model we
utilize a student-teacher architecture similar to those in distillation methods
\cite{hinton2015distilling, furlanello2016active}. The teacher contains a
summary of all past distributions and is used to augment the data used to train the
student model. The student model thus receives data samples from the currently observable
distribution as well as synthetic data samples from previous
distributions. % This allows the student model to learn a distribution that summarizes the current
% as well as all previously observed distributions.
Once a distribution shift occurs the
existing teacher model is \emph{discarded}, the student becomes the teacher and a new
student is instantiated.
\setlength{\textfloatsep}{5pt}
\begin{figure*}% [ht]
\vskip -0.3in
\begin{center}
  % \centerline{\includegraphics[width=150mm]{./imgs/distribution_changes_2d_latent_joint2}}
  % \centerline{\includegraphics[width=\textwidth]{./imgs/distribution_changes_2d_latent_joint2}}
  \scalebox{0.8}{\centerline{\includegraphics[height=43mm]{./imgs/distribution_changes_2d_latent_joint3}}}
  % \captionsetup{justification=centering}
  % \setlength{\belowcaptionskip}{-10pt}
\caption{(a) Our problem setting where we sequentially
  observe samples from multiple unknown distributions; (b)Visualization of a learnt two-dimensional posterior of MNIST, evaluated with samples
  from the full test set. We depict the two generative shortcomings
  visually: 1) mixing of distributions which causes aliasing in a lifelong
  setting and 2) undersampling of distributions in a standard VAE
  posterior. }
\label{fig_problem_setup}
\end{center}
\vskip -0.1in
\end{figure*}
% If we assume that our teacher has learnt a good distribution to summarize the
% data it was exposed to, it would be apt to leverage this representation in the
% student model. % In order accomplish this, we introduce a regularizer that brings the posterior
% distribution of the student close to that of the teacher. % This
% is only done for previously learnt distributions and not for the currently
% unobserved distribution.

We introduce a novel regularizer in the form of a Bayesian update rule
that allows us to bring the posterior of the student close to that of
the teacher for the synthetic data generated by the teacher.
% We further leverage the generative model of the teacher by introducing a regularizer in the learning objective function of the student
% that brings the posterior distribution of the latter close to that of the former.
This allows us to build upon and extend the teacher's inference model into the student each time
the latter is re-instantiated (rather than re-learning it from scratch).
By coupling this regularizer with a weight transfer from the teacher to the student we also allow for faster
convergence of the student model. We empirically show that this
regularizer mitigates the effect of catastrophic interference
\cite{mccloskey1989catastrophic}. It also ensures that even though our
model evolves over time, it preserves the ability to generate samples
from any of the previously observed distributions, a property we call \emph{consistent
sampling}.

% This allows us to build upon/finetune the teacher's learnt representation, rather than re-learn an entirely new representation
% for the student model. By coupling our proposed regularizer with an initial
% weight transfer from the teacher to the student we further restrict the
% search space of the hypothesis class and allow for faster convergence of the
% student model. We empirically show that utilizing the proposed
% regularizer allows us to learn a much larger set of distributions without
% catastrophic interference \cite{mccloskey1989catastrophic}.

% We build our lifelong generative models over Variational Autoencoders (VAEs)
% \cite{kingma2014}. VAEs learn the posterior distribution of a latent variable
% model using an encoder network; they generate data by sampling from a prior and
% decoding the sample via a conditional distribution learned by a decoder network.
% Using a vanilla VAE as a teacher to generate synthetic data for the student is
% problematic due to a couple of limitations of the VAE generative process.
% Sampling the prior can select a point in the latent space that is in between two
% separate distributions, causing generation of unrealistic synthetic data and
% eventually leading to loss of previously learnt distributions. Additionally,
% data points mapped to the posterior that are further away from the prior mean
% will be sampled less frequently resulting in an unbalanced sampling of the
% constituent distributions. Both limitations can be understood by visually
% inspecting the learnt posterior distribution of a standard VAE evaluated on test
% images from MNIST as shown in figure~\ref{fig_problem_setup}(b). To address
% the VAE's sampling limitations described above we decompose the latent variable
% vector to a continuous and a discrete component. The discrete component
% identifies and indexes the individual generative distributions seen throughout
% the streaming history. The continuous component caters for the sample
% variability. By independently sampling the discrete and continuous components we
% preserve the distributional boundaries and circumvent the two problems described
% above. In addition, this strategy allows us to generate data from any given past
% distribution, a property we call consistent sampling; we are able to do this
% even though the complete marginal distribution evolves over time and captures
% the full stream history.

We choose to build our lifelong generative models using Variational Autoencoders (VAEs)
\cite{kingma2014} as they provide an efficient and stable way to do
posterior inference, a requirement for our regularizer and data
generation method (Section \ref{regularizer}). % VAEs learn the posterior distribution of a latent variable
% model using an encoder network; they generate data by sampling from a prior and
% decoding the sample through a conditional distribution learnt by a decoder network.
Using a standard VAE decoder to generate synthetic data for the student is
problematic due to a couple of limitations of the VAE generative
process as shown in Figure \ref{fig_problem_setup}(b). 1) Sampling the prior can select a point in the latent space that is in between two
separate distributions, causing generation of unrealistic synthetic data and
eventually leading to loss of previously learnt distributions; 2) data points mapped to the posterior that are further away from the prior mean
will be sampled less frequently resulting in an undersampling of some % unbalanced sampling
of the constituent distributions\footnote{This is due to the fact that VAE's
  generate data by sampling their prior and decoding the
  sample through the decoder neural network. Thus a posterior instance further from
  the prior mean is sampled less frequently.}. % Both limitations can be understood by visually
% inspecting the learnt posterior distribution of a standard VAE evaluated on test
% images from MNIST as shown in figure~\ref{fig_problem_setup}(b).
To address these sampling limitations we decompose the latent variable
vector into a continuous and a discrete component (Section \ref{sampling}). % This is similar to
% the M2 model proposed in \cite{kingma2014semi} with the difference
% that we are working in a purely unsupervised setting.
The discrete component summarizes the discriminative
information  of the individual generative distributions while the continuous
caters for the remaining sample variability (% also commonly known as
a nuisance variable \cite{louizos2015variational}). By independently sampling the
discrete and continuous components we preserve the distributional boundaries and
circumvent the two VAE limitations described above.

% This sampling strategy, combined with the
% proposed regularizer allows us to learn and remember all the individual
% distributions observed in the past. In addition we are also able to generate
% samples from any of the past distributions at will; we call this property
% consistent sampling.

% This sampling strategy, combined
% with the proposed regularizer allows us generate data from the individual
% distributions observed in the past, even though the complete marginal distribution
% $P_{\bm{\theta}}(\bm{\mathrm{x}})$ evolves over time and captures the full
% stream history. We call this property consistent sampling.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "online_gm_icml"
%%% End:
